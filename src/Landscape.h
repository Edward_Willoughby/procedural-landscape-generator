/********************************************************************
*
*	CLASS		:: Landscape
*	DESCRIPTION	:: Holds the data about the landscape.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 22
*
********************************************************************/

#ifndef LandscapeH
#define LandscapeH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <vector>

#include <DBEMovable.h>
#include <DBERenderable.h>

namespace DBE {
	class BasicMesh;
}
struct VertPos3fColour4ubNormal3f;
/********************************************************************
*	Defines and constants.
********************************************************************/
enum VertPos {
	VP_Above = 0,
	VP_Below,
	VP_Left,
	VP_Right,
	VP_AboveLeft,
	VP_AboveRight,
	VP_BelowLeft,
	VP_BelowRight,
	VP_Count,
};

struct LandscapeSettings {
	LandscapeSettings()
		: width( 0)
		, height( 0)
		, hilly( 0)
	{}

	s32 width;
	s32 height;

	s32 hilly;
};

struct Tree : public DBE::Movable {
	Tree()
		: m_birth( 0)
		, m_age( 0)
	{}

	s32 m_birth;
	s32 m_age;
};


/*******************************************************************/
class Landscape : public DBE::Movable {
	public:
		/// Constructor.
		Landscape();
		/// Destructor.
		~Landscape();

		/// Updates the landscape.
		bool Update( float deltaTime);
		/// Renders the landscape.
		void Render( float deltaTime);

		/// Creates a landscape based on the new settings.
		void GenerateLandscape();
		/// Deletes the landscape.
		void DeleteLandscape();

		/// Sets the details on what the new landscape should contain.
		void SetLandscapeSettings( const LandscapeSettings& s);

		/// Develop the landscape by the specified value.
		void AgeLandscape( s32 age);
		/// Gets the age of the landscape.
		s32 GetLandscapesAge() const;
		
		/// Gradually develop the landscape by the specified value.
		void AgeLandscapeOverTime( s32 age);
		/// Stops any gradual aging.
		void StopAging();

		/// Gets the vertex position at the specified array position.
		DBE::Vec3 GetVertPos( s32 h, s32 w);

		/// Find where a point above the heightmap collides with the heightmap.
		bool FindHeightmapPoint( const DBE::Vec3& origin, DBE::Vec3& colPos);
		
	private:
		/// Creates the heightmap's verticies.
		void GenerateHeightmap();
		/// Plants initial trees on the heightmap.
		void GenerateTrees();

		/// Test if a tree is old enough to produce offspring.
		bool TreeIsOfGerminationAge( const Tree* tree) const;
		/// Plant trees around the specified tree.
		void NewTreeGeneration( const Tree* tree);

		/// Add a new tree at the specified position.
		void AddNewTree( const DBE::Vec3& treePos);

		/// Gets a random position on the heightmap.
		void GetRandomPosition( s32& w, s32& h);
		/// Move a vertex on the heightmap upwards.
		void MoveVertexUp( VertPos3fColour4ubNormal3f* verts, s32 pos, s32 max, float height);
		/// Get the index of a vertex next to the specified one.
		s32 GetVertexIndex( s32 index, VertPos vp);
		/// Gets a vertex next to the specified one.
		VertPos3fColour4ubNormal3f* GetVertex( VertPos3fColour4ubNormal3f* verts, s32 index, VertPos vp);

		typedef std::vector<Tree*> VectorOfTrees;

		LandscapeSettings m_settingsNew;
		LandscapeSettings m_settingsCurrent;

		DBE::BasicMesh* mp_treeMesh;	// The tree mesh that all trees are rendered from.

		DBE::BasicMesh*	mp_heightmap;
		s32*			mp_fertility;
		VectorOfTrees	mp_trees;

		u32 m_age;
		u32 m_amountToAge;

		u32 m_indexTest;

		/// Private copy constructor to prevent accidental multiple instances.
		Landscape( const Landscape& other);
		/// Private assignment operator to prevent accidental multiple instances.
		Landscape& operator=( const Landscape& other);
		
};

/*******************************************************************/
#endif	// #ifndef LandscapeH
