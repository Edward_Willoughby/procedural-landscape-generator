/********************************************************************
*	Function definitions for the Landscape class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Landscape.h"

#include <DBEApp.h>
#include <DBEBasicMesh.h>
#include <DBEGraphicsHelpers.h>
#include <DBEMath.h>
#include <DBERandom.h>
#include <DBEVertexTypes.h>

#include "PerlinNoise.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

/// Constant values for heights of various heightmap contours.
static const float HEIGHT_HILL_PEAK = 1.5f;
static const float HEIGHT_PLATEAU_PEAK = 1.5f;
static const float HEIGHT_MOUNTAIN_PEAK = 6.0f;

/// Constant values for fertility.
static const s32 FERTILE_SOURCE = -1;
static const float TREE_EXCLUSION_ZONE = 1.5f;
static const s32 TREE_GERMINATION_START = 20;
static const s32 TREE_GERMINATION_REST = 5;
static const s32 TREE_BARREN = TREE_GERMINATION_START + 20;
static const s32 MAX_SEEDS = 10;

/// Path for the music file.
const char* gs_musicGrowing = "res/audio/music_carly_commando_everyday.mp3";


/**
* Constructor.
*/
Landscape::Landscape() {
	mp_treeMesh = new BasicMesh( MT_Cone, 0.05f, 0.0f, 1.0f);

	mp_heightmap = new BasicMesh( MT_Plane, 2.0f, 2.0f, 0.0f);
	//mp_heightmap->RotateTo( DBE_ToRadians( 90.0f), 0.0f, 0.0f);
	//mp_heightmap->MoveTo( 0.0f, -0.5f, 0.0f);

	mp_fertility = nullptr;

	m_age = 0;
	m_amountToAge = 0;

	MGR_AUDIO().LoadAudio( gs_musicGrowing);
}

/**
* Destructor.
*/
Landscape::~Landscape() {
	this->DeleteLandscape();
}

/**
* Updates the landscape.
*
* @param deltaTime :: Time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool Landscape::Update( float deltaTime) {
	float s( 1.01f);
	//mp_tree->ScaleBy( s, s, s);
	mp_treeMesh->Update( deltaTime);

	if( MGR_INPUT().KeyPressed( VK_OEM_PERIOD)) {
		if( mp_heightmap->m_indexCount < m_indexTest)
			mp_heightmap->m_indexCount += 3;
	}
	else if( MGR_INPUT().KeyPressed( VK_OEM_COMMA)) {
		if( mp_heightmap->m_indexCount > 0)
			mp_heightmap->m_indexCount -= 3;
	}

	if( MGR_INPUT().KeyPressed( VK_OEM_PLUS))
		this->AgeLandscapeOverTime( 100);
	else if( MGR_INPUT().KeyPressed( VK_OEM_MINUS))
		this->StopAging();

	static float time = 0.0f;
	static float timePerYear = 0.5f;
	if( m_amountToAge != 0) {
		time += deltaTime;

		if( time > timePerYear) {
			time -= timePerYear;

			m_amountToAge -= 1;
			this->AgeLandscape( 1);

			if( m_amountToAge > 0) {
				if( !MGR_AUDIO().IsPlaying( gs_musicGrowing))
					MGR_AUDIO().PlayAudio( gs_musicGrowing);
			}
			else {
				if( MGR_AUDIO().IsPlaying( gs_musicGrowing))
					MGR_AUDIO().StopAudio( gs_musicGrowing);
			}
		}
	}

	mp_heightmap->Update( deltaTime);

	return true;
}

/**
* Renders the landscape.
*
* @param deltaTime :: Time taken to render the previous frame.
*/
void Landscape::Render( float deltaTime) {
	// Don't try to render an empty landscape.
	if( m_settingsCurrent.width <= 0 && m_settingsCurrent.height <= 0)
		return;

	mp_heightmap->Render( deltaTime);

	for( VectorOfTrees::iterator it( mp_trees.begin()); it != mp_trees.end(); ++it) {
		Tree* p_tree( *it);
		mp_treeMesh->RotateTo( p_tree->GetRotation());

		// Scale the tree to suit the age of the tree.
		float scale( 0.0f);
		if( p_tree->m_age <= TREE_GERMINATION_START)
			scale = float( p_tree->m_age) / 4.0f;
		else
			scale = float( TREE_GERMINATION_START) / 4.0f;

		mp_treeMesh->ScaleTo( scale);
		
		// I currently have to work out a correct position for the tree because its center is at
		// the center of the mesh, instead of at the base on the trunk: hence the offset upwards.
		Vec3 pos( p_tree->GetPosition());
		float halfYScale( scale / 2);

		mp_treeMesh->MoveTo( pos.GetX(), pos.GetY() + halfYScale, pos.GetZ());

		mp_treeMesh->Update( deltaTime);
		mp_treeMesh->Render( deltaTime);
	}
}

/**
* Creates a landscape based on the new settings.
*/
void Landscape::GenerateLandscape() {
	this->DeleteLandscape();

	m_settingsCurrent = m_settingsNew;
	m_age = 0;

	this->GenerateHeightmap();
	this->GenerateTrees();
}

/**
* Deletes the landscape.
*/
void Landscape::DeleteLandscape() {
	// Delete the heightmap.
	ReleaseCOM( mp_heightmap->mp_vertBuffer);
	ReleaseCOM( mp_heightmap->mp_indexBuffer);
	SafeDeleteArray( mp_heightmap->mp_verts);
	SafeDeleteArray( mp_heightmap->mp_indicies);

	// Delete the fertility array.
	SafeDeleteArray( mp_fertility);

	// Delete the trees.
	for( VectorOfTrees::iterator it( mp_trees.begin()); it != mp_trees.end(); ++it)
		SafeDelete( *it);
	mp_trees.clear();
}

/**
* Sets the details on what the new landscape should contain.
*
* @param s :: The settings that should be used for the next landscape to be generated.
*/
void Landscape::SetLandscapeSettings( const LandscapeSettings& s) {
	m_settingsNew = s;
}

/**
* Develop the landscape by the specified value.
*
* @param age :: How much to age the landscape by.
*/
void Landscape::AgeLandscape( s32 age) {
	// The landscape can only be aged, not youthed.
	if( age < 0)
		return;

	while( age > 0) {
		m_age += 1;

		// New tree may be added during this loop, hence the use of an index rather than an
		// iterator.
		for( s32 i( 0); i < mp_trees.size(); ++i) {
			Tree* p_tree( mp_trees[i]);

			// Ignore trees that have been 'planted' during this iteration.
			if( p_tree->m_birth == m_age)
				continue;

			p_tree->m_age += 1;

			// If the tree is at a time that it should germinate, then create a new generation of
			// trees around the parent tree.
			if( this->TreeIsOfGerminationAge( p_tree))
				this->NewTreeGeneration( p_tree);
		}

		age -= 1;
	}
}

/**
* Gets the age of the landscape.
*
* @return The landscape's age.
*/
s32 Landscape::GetLandscapesAge() const {
	return m_age;
}

/**
* Gradually develop the landscape by the specified value.
*
* @param The amount to age the landscape by.
*/
void Landscape::AgeLandscapeOverTime( s32 age) {
	m_amountToAge += age;
}

/**
* Stops any gradual aging.
*/
void Landscape::StopAging() {
	m_amountToAge = 0;
}

/**
* Gets the vertex position at the specified array position.
*
* @param h :: The height position of the vertex.
* @param w :: The width position of the vertex.
*
* @return The 3D position of the vertex.
*/
DBE::Vec3 Landscape::GetVertPos( s32 h, s32 w) {
	return mp_heightmap->mp_verts[(h * (m_settingsCurrent.height + 1)) + w].pos;
}

/**
* Find where a point above the heightmap collides with the heightmap. The direction always set to
* be downwards, so if the point is below the heightmap then it won't collide. To ensure it does,
* set the 'y' of the 'origin' to something that is definitely higher than the highest point on the
* heightmap.
*
* @param origin	:: The point above the heightmap.
* @param colPos	:: The point where the ray collides with the heightmap. Unchanged if the ray
*					doesn't collide with the heightmap.
*
* @return True if the ray collided with the heightmap.
*/
bool Landscape::FindHeightmapPoint( const DBE::Vec3& origin, DBE::Vec3& colPos) {
	Vec3 dir( 0.0f, -1.0f, 0.0f);

	for( u32 i( 0); i < mp_heightmap->m_indexCount; i += 3) {
		Vec3 v0( mp_heightmap->mp_verts[mp_heightmap->mp_indicies[i+0]].pos);
		Vec3 v1( mp_heightmap->mp_verts[mp_heightmap->mp_indicies[i+1]].pos);
		Vec3 v2( mp_heightmap->mp_verts[mp_heightmap->mp_indicies[i+2]].pos);

		float dist( 0.0f);
		if( Collision::IntersectRayTriangle( origin, dir, v0, v1, v2, &dist)) {
			colPos = origin + (dir * dist);
			return true;
		}
	}

	return false;
}

/**
* Creates the heightmap's verticies.
*/
void Landscape::GenerateHeightmap() {
	s32 height( m_settingsCurrent.height);
	s32 width( m_settingsCurrent.width);
	s32 tileCount( height * width);

	// Create a basic flat ground.
	s32 vertCount( (height+1) * (width+1));
	s32 indexCount( ((height * width) * 2) * 3);
	PerlinNoise pn( Random::Int( 1, 255));

	VertPos3fColour4ubNormal3f* verts = new VertPos3fColour4ubNormal3f[vertCount];
	UINT* indicies = new UINT[indexCount];

	s32 count( 0);
	float hOffset( float( height) / 2);
	float wOffset( float( width) / 2);
	for( s32 i( 0); i <= height; ++i) {
		for( s32 j( 0); j <= width; ++j) {
			float y( pn.Noise( i * 10, j * 10, 0.8));
			verts[count] = VertPos3fColour4ubNormal3f( Vec3( j - wOffset, y, i - hOffset), RED, Vec3( 0.0f, 1.0f, 0.0f));
			++count;
		}
	}

	s32 index( 0);
	s32 mapIndex( 0);
	for( s32 i( 0); i < height; ++i, ++mapIndex) {
		for( s32 j( 0); j < width; ++j, ++mapIndex) {
			indicies[index+0] = mapIndex;
			indicies[index+1] = mapIndex + width + 1;
			indicies[index+2] = mapIndex + 1;
			indicies[index+3] = mapIndex + 1;
			indicies[index+4] = mapIndex + width + 1;
			indicies[index+5] = mapIndex + width + 2;

			index += 6;
		}
	}

	// Raise certain areas to give hills/mountians.
	s32 hillCount( DBE_Ceil( float( tileCount) / 100));
	s32 plateauCount( DBE_Ceil( float( tileCount) / 500));
	s32 mountainCount( DBE_Ceil( float( tileCount) / 500));
	s32 w, h;
	
	//hillCount = 0;
	//plateauCount = 0;
	//mountainCount = 0;

	// Create the hills.
	for( s32 i( 0); i < hillCount; ++i) {
		this->GetRandomPosition( w, h);
		s32 pos( (h * (height + 1)) + w);

		// Create the peak.
		this->MoveVertexUp( verts, pos, vertCount, HEIGHT_HILL_PEAK);

		float slopeHeight( HEIGHT_HILL_PEAK / 2);

		// Row above.
		if( h > 0) {
			if( w > 0)
				this->MoveVertexUp( verts, pos - (height + 1 + 1), vertCount, slopeHeight);
			if( w < width)
				this->MoveVertexUp( verts, pos - (height + 1 - 1), vertCount, slopeHeight);

			this->MoveVertexUp( verts, pos - (height + 1 + 0), vertCount, slopeHeight);
		}

		// Row below.
		if( h < height) {
			if( w > 0)
				this->MoveVertexUp( verts, pos + (height + 1 - 1), vertCount, slopeHeight);
			if( w < width)
				this->MoveVertexUp( verts, pos + (height + 1 + 1), vertCount, slopeHeight);

			this->MoveVertexUp( verts, pos + (height + 1 + 0), vertCount, slopeHeight);
		}
		
		// And the same row.
		if( w > 0)
			this->MoveVertexUp( verts, pos - 1, vertCount, slopeHeight);
		if( w < width)
			this->MoveVertexUp( verts, pos + 1, vertCount, slopeHeight);

		//DebugTrace( "h: %i, w: %i\n", h, w);
	}


	// Set the fertility of each 'point' on the heightmap: the more fertile an area is the more
	// likely trees will thrive there.
	mp_fertility = new s32[vertCount];
	for( s32 h( 0); h <= height; ++h) {
		for( s32 w( 0); w <= width; ++w) {
			s32 index( h * (height+1) + w);
			float vertHeight( verts[index].pos.GetY());

			// If the vertex is underwater...
			if( vertHeight < 0.0f) {
				mp_fertility[index] = FERTILE_SOURCE;
				continue;
			}

			// ... If not, then calculate a value for it based on the surrounding verticies.
			float fertility( Lerp( 50.0f, 0.0f, vertHeight / HEIGHT_MOUNTAIN_PEAK));
			Vec3* neighbours[3][3];

			neighbours[0][0] = &GetVertex( verts, index, VP_AboveLeft)->pos;
			neighbours[0][1] = &GetVertex( verts, index, VP_Above)->pos;
			neighbours[0][2] = &GetVertex( verts, index, VP_AboveRight)->pos;
			neighbours[1][0] = &GetVertex( verts, index, VP_Left)->pos;
			neighbours[1][1] = nullptr;		// The central vertex.
			neighbours[1][2] = &GetVertex( verts, index, VP_Right)->pos;
			neighbours[2][0] = &GetVertex( verts, index, VP_BelowLeft)->pos;
			neighbours[2][1] = &GetVertex( verts, index, VP_Below)->pos;
			neighbours[2][2] = &GetVertex( verts, index, VP_BelowRight)->pos;

			for( s32 i( 0); i < 3 * 3; ++i) {
				if( neighbours[0][i] == nullptr)
					continue;

				if( neighbours[0][i]->GetY() > vertHeight)
					fertility += 5.0f;
				else if( neighbours[0][i]->GetY() < vertHeight)
					fertility -= 5.0f;
			}

			mp_fertility[index] = Clamp<float>( fertility, 0, 100);
		}
	}

	// Fix the normals for each vertex by taking an average of the four surrounding faces.
	for( s32 h( 0); h <= height; ++h) {
		for( s32 w( 0); w <= width; ++w) {
			s32 vertPos( h * (height+1) + w);
			Vec3 norm( 0.0f);

			for( s32 index( 0); index < indexCount; index += 3) {
				s32 v0( indicies[index+0]), v1( indicies[index+1]), v2( indicies[index+2]);
				if( v0 != vertPos && v1 != vertPos && v2 != vertPos)
					continue;

				Vec3 vec1( verts[v0].pos - verts[v1].pos);
				Vec3 vec2( verts[v0].pos - verts[v2].pos);
				
				vec1 = Cross( vec1, vec2);
				norm += vec1;
			}

			verts[vertPos].normal = norm.GetNormalised();
		}
	}

	// Set the colour of each vertex based on its elevation.
	// I may, later, slightly change the colour based on their fertilisation as well, however this
	// would have to be updated each frame which would be REALLY inefficient.
	u32 colourGrass( 0x73e933FF);
	u32 colourHill( 0x508125FF);
	u32 colourMountain( 0xFFFFFFFF);
	for( s32 h( 0); h <= height; ++h) {
		for( s32 w( 0); w <= width; ++w) {
			s32 vertPos( h * (height+1) + w);
			float vertHeight( verts[vertPos].pos.GetY());
			u8 f[4], s[4], r[4];
			float higherGround( 0.0f);

			if( vertHeight > HEIGHT_MOUNTAIN_PEAK) {
				verts[vertPos].colour = colourMountain;
				continue;
			}
			else if( vertHeight > HEIGHT_MOUNTAIN_PEAK / 2) {
				higherGround = HEIGHT_MOUNTAIN_PEAK;
				ColourToInts( f[0], f[1], f[2], f[3], colourHill);
				ColourToInts( s[0], s[1], s[2], s[3], colourMountain);
			}
			else if( vertHeight >= 0.0f && vertHeight <= HEIGHT_MOUNTAIN_PEAK / 2) {
				higherGround = HEIGHT_MOUNTAIN_PEAK / 2;
				ColourToInts( f[0], f[1], f[2], f[3], colourGrass);
				ColourToInts( s[0], s[1], s[2], s[3], colourHill);
			}
			else if( vertHeight < 0.0f) {
				continue;
			}

			for( u8 i( 0); i < 4; ++i)
				r[i] = Lerp( f[i], s[i], vertHeight / higherGround);

			verts[vertPos].colour = VertColour( NumbersToColour( r[0], r[1], r[2], r[3]));
		}
	}
	
	// Create the buffers and store the required information in the heightmap variable.
	mp_heightmap->mp_verts = verts;
	mp_heightmap->m_vertexCount = vertCount;
	mp_heightmap->mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3f) * vertCount, mp_heightmap->mp_verts);

	mp_heightmap->mp_indicies = indicies;
	mp_heightmap->m_indexCount = indexCount;
	mp_heightmap->mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT) * indexCount, indicies);

	m_indexTest = indexCount;
}

/**
* Plants initial trees on the heightmap.
*/
void Landscape::GenerateTrees() {
	s32 height( m_settingsCurrent.height);
	s32 width( m_settingsCurrent.width);
	s32 fertCount( (height+1) * (width+1));
	std::vector<std::pair<s32, s32>> orderedFert;

	orderedFert.reserve( fertCount);

	// Order the fertility ratings so the top locations will be selected.
	for( s32 i( 0); i < fertCount; ++i)
		orderedFert.push_back( std::pair<s32, s32>( mp_fertility[i], i));
	std::sort( orderedFert.rbegin(), orderedFert.rend());

	// Now select the top three locations.
	s32 treeCount( 3);
	for( s32 i( 0); i < treeCount; ++i) {
		s32 fertileIndex( 0);
		Vec3 fertilePos;

		bool badPosition( true);
		while( badPosition) {
			// Select the best position (taking into account the fertility, light, etc.).
			s32 j( 0);
			for( j; j < fertCount; ++j) {
				if( orderedFert[0].first == orderedFert[j].first)
					continue;

				--j;
				break;
			}
			fertileIndex = Random::Int( 0, j);
			fertilePos = mp_heightmap->mp_verts[orderedFert[fertileIndex].second].pos;

			// Whether it's a good or bad position, I don't want it in the vector anymore.
			orderedFert.erase( orderedFert.begin() + fertileIndex);

			// Check that the position is viable (i.e. not too close to other trees).
			bool stillBad( false);
			for( VectorOfTrees::iterator it( mp_trees.begin()); it != mp_trees.end(); ++it) {
				Vec3 vDist( (*it)->GetPosition() - fertilePos);
				float dist( vDist.GetLength());

				if( dist < TREE_EXCLUSION_ZONE) {
					stillBad = true;
					break;
				}
			}

			if( !stillBad)
				badPosition = false;
		}

		// After finding a suitable spot for the tree, create the tree and add it to the vector.
		this->AddNewTree( fertilePos);
	}
}

/**
* Test if a tree is old enough to produce offspring.
*
* @param tree :: Pointer to the tree that will produce new trees.
*
* @return True if the tree should produce seeds at its current age.
*/
bool Landscape::TreeIsOfGerminationAge( const Tree* tree) const {
	s32 age( tree->m_age);
	// Early out if the tree isn't of germination age yet.
	if( age < TREE_GERMINATION_START)
		return false;

	// Early out if the tree is beyond germination age.
	if( age > TREE_BARREN)
		return false;

	// Calculate whether it's a 'germination age' for the tree.
	age = age - TREE_GERMINATION_START;
	age = age % TREE_GERMINATION_REST;

	// If there's a remainder, then it's not time to germinate.
	if( age != 0)
		return false;

	return true;
}

/**
* Plant trees around the specified tree.
*
* @param tree :: The tree that the seeds will sprout from.
*/
void Landscape::NewTreeGeneration( const Tree* tree) {
	Vec3 parentPos( tree->GetPosition().GetX(), 0.0f, tree->GetPosition().GetZ());
	Vec3 seeds[MAX_SEEDS];
	s32 survival[MAX_SEEDS];

	// Drop seeds at random positions around the base of the tree.
	for( s32 i( 0); i < MAX_SEEDS; ++i) {
		float angle( DBE_ToRadians( Random::Float( 360.0f)));
		float randDist( Random::Float( 0.0f, float( TREE_EXCLUSION_ZONE) * 0.1f));
		float x( 0.0f), z( 0.0f);

		DirectX::XMScalarSinCos( &x, &z, angle);

		seeds[i] = parentPos + (Vec3( x, 0.0f, z) * (float( TREE_EXCLUSION_ZONE) + randDist));

		// Initialise the survival array.
		survival[i] = 0;
	}

	// Take away any that can't survive due to existing trees.
	for( s32 i( 0); i < MAX_SEEDS; ++i) {
		bool wouldDie( false);

		for( VectorOfTrees::iterator it( mp_trees.begin()); it != mp_trees.end(); ++it) {
			Vec3 treePos( (*it)->GetPosition());
			treePos.SetY( 0.0f);

			float dist( Distance( seeds[i], treePos));

			if( dist >= TREE_EXCLUSION_ZONE)
				continue;

			wouldDie = true;
			break;
		}

		if( wouldDie)
			survival[i] = -1;
	}

	// Calculate the survival chance for each seed.
	for( s32 i( 0); i < MAX_SEEDS; ++i) {
		// Skip this seed if it's already known to be going to die.
		if( survival[i] == -1)
			continue;

		s32 rating( 0);

		for( s32 index( 0); index < mp_heightmap->m_vertexCount; ++index) {
			// Ignore any height differences.
			Vec3 vertPos( mp_heightmap->mp_verts[index].pos);
			vertPos.SetY( 0.0f);

			// Get the seed's distance from the fertility point.
			float dist( Distance( seeds[i], vertPos));

			// Ignore the point if it's too far away to provide nutrients.
			if( dist > TREE_EXCLUSION_ZONE)
				continue;

			// Otherwise, take a certain amount of the point's fertility rating based on its
			// distance from the seed.
			s32 fert( mp_fertility[index]);
			if( fert == -1)
				fert = 100;

			fert = Lerp( float( fert), 0.0f, dist / TREE_EXCLUSION_ZONE);
			rating += fert;
		}

		survival[i] = rating;
	}

	// Select the most likely to survive.
	s32 seedsToSurvive( Random::Int( 1, 3));
	s32 bestChance( -1);
	for( s32 s( 0); s < seedsToSurvive; ++s) {
		// If this is not the first iteration (i.e. another seed has been planted) then remove any
		// potential seeds that are too close to that new seed. The latest seed's index should
		// still be preserved in the 'bestChance' variable.
		if( bestChance != -1) {
			for( s32 i( 0); i < MAX_SEEDS; ++i) {
				// Don't compare the seed to itself.
				if( bestChance == i)
					continue;

				bool tooClose( false);
				Vec3 seedPos( seeds[i]);
				Vec3 seedBest( seeds[bestChance]);

				seedPos.SetY( 0.0f);
				seedBest.SetY( 0.0f);

				float dist( Distance( seedBest, seedPos));

				if( dist < TREE_EXCLUSION_ZONE)
					survival[i] = -1;
			}

			// Now get rid of the previous seed's survival rating so it's not selected again.
			survival[bestChance] = -1;
		}
		else {
			bestChance = 0;
		}

		// Find the seed with the best chance to survive.
		for( s32 i( 1); i < MAX_SEEDS; ++i)
			if( survival[i] > survival[bestChance])
				bestChance = i;

		// Early out if there are no seeds with a chance of surviving.
		if( survival[bestChance] == -1)
			break;

		// Ensure the tree is positioned at the correct height on the heightmap.
		// If the function returns 'false' then it means the seed was dropped off the heightmap.
		Vec3 colPos;
		Vec3 origin( seeds[bestChance].GetX(), HEIGHT_MOUNTAIN_PEAK + 1.0f, seeds[bestChance].GetZ());
		if( this->FindHeightmapPoint( origin, colPos)) {
			// Seeds can't be planted too far above the parent tree: they'd roll down into the
			// parent's exclusion zone.
			if( colPos.GetY() - 0.75f <= tree->GetPosition().GetY())
				this->AddNewTree( colPos);
		}

	}
}

/**
* Add a new tree at the specified position. The position sent should already have had checking done
* to it to make sure that the tree is in a valid location.
*
* @param treePos :: The position to plant the tree.
*/
void Landscape::AddNewTree( const DBE::Vec3& treePos) {
	Tree* p_tree = new Tree();

	p_tree->MoveTo( treePos);
	p_tree->m_birth = m_age;

	mp_trees.push_back( p_tree);
}

/**
* Gets a random position on the heightmap.
*
* @param w :: The width position.
* @param h :: The height position.
*/
void Landscape::GetRandomPosition( s32& w, s32& h) {
	w = Random::Int( m_settingsCurrent.width + 1);
	h = Random::Int( m_settingsCurrent.height + 1);
}

/**
* Move a vertex on the heightmap upwards. Note that this ONLY moves verticies upwards: so if the
* vertex is already higher than the value specified by 'height' then it will remain where it is,
* instead of being brought down to the value of 'height'.
*
* @param verts	:: The array of verticies.
* @param pos	:: The position of the vertex.
* @param max	:: The number of verticies in 'verts'.
* @param height	:: The height to move to vertex to.
*/
void Landscape::MoveVertexUp( VertPos3fColour4ubNormal3f* verts, s32 pos, s32 max, float height) {
	if( pos < 0 || pos >= max)
		return;

	Vec3* vec( &verts[pos].pos);
	if( vec->GetY() < height)
		vec->SetY( height);
}

/**
* Get the index of a vertex next to the specified one.
*
* @param index	:: The position of the vertex in the array of verticies.
* @param vp		:: Which vertex to acquire (i.e. which direction it is from the 'index' vertex).
*					Refer to the 'VertPos' enum for the available directions.
*/
s32 Landscape::GetVertexIndex( s32 index, VertPos vp) {
	s32 h( index / (m_settingsCurrent.height + 1));
	s32 w( DBE_Mod( index, (m_settingsCurrent.height + 1)));

	if( vp == VP_Above || vp == VP_AboveLeft || vp == VP_AboveRight)
		--h;
	else if( vp == VP_Below || vp == VP_BelowLeft || vp == VP_BelowRight)
		++h;

	if( vp == VP_Left || vp == VP_AboveLeft || vp == VP_BelowLeft)
		--w;
	else if( vp == VP_Right || vp == VP_AboveRight || vp == VP_BelowRight)
		++w;

	s32 maxHeight( m_settingsCurrent.height);
	s32 maxWidth( m_settingsCurrent.width);
	if( h < 0 || h > maxHeight || w < 0 || w > maxWidth)
		return -1;

	index = h * (maxHeight + 1) + w;

	return index;
}

/**
* Gets a vertex next to the specified one.
*
* @param verts	:: The array of verticies.
* @param index	:: The position of the vertex in the 'verts' array.
* @param vp		:: Which vertex to acquire (i.e. which direction it is from the 'index' vertex).
*					Refer to the 'VertPos' enum for the available directions.
*
* @return The vertex in the 'verts' array that's next to 'index'. If the desired vertex is off the
*			heightmap, then it will return nullptr.
*/
VertPos3fColour4ubNormal3f* Landscape::GetVertex( VertPos3fColour4ubNormal3f* verts, s32 index, VertPos vp) {
	index = this->GetVertexIndex( index, vp);

	if( index == -1)
		return nullptr;

	return &verts[index];
}