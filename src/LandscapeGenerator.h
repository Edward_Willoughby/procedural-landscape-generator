/********************************************************************
*
*	CLASS		:: LandscapeGenerator
*	DESCRIPTION	:: A small program to procedurally generator landscapes based on user-input values.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 22
*
********************************************************************/

#ifndef LandscapeGeneratorH
#define LandscapeGeneratorH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEMath.h>

namespace DBE {
	class BasicMesh;
	class Camera;
	class Font;
}
class Landscape;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class LandscapeGenerator : public DBE::App {
	public:
		/// Constructor.
		LandscapeGenerator() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with any 2D rendering.
		void HandleRender2D( float deltaTime);
		/// Deals with any 3D rendering.
		void HandleRender3D( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		const char* m_windowTitle;

		DBE::Camera*	mp_cam;
		float			m_phi;
		bool			m_rotateCam;
		bool m_wireframe;

		Landscape* mp_landscape;

		DBE::BasicMesh* mp_test;
		DBE::BasicMesh* mp_test2;

		/// Private copy constructor to prevent multiple instances.
		LandscapeGenerator( const LandscapeGenerator&);
		/// Private assignment operator to prevent multiple instances.
		LandscapeGenerator& operator=( const LandscapeGenerator&);

};

APP_MAIN( LandscapeGenerator, BLUE);

/*******************************************************************/
#endif	// #ifndef LandscapeGeneratorH