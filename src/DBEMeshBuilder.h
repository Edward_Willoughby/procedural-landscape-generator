/********************************************************************
*
*	CLASS		:: DBE::MeshBuilder
*	DESCRIPTION	:: Stores verticies and indicies so a mesh can be created.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 22
*
********************************************************************/

#ifndef DBEMeshBuilderH
#define DBEMeshBuilderH

/********************************************************************
*	Include libraries and header files.
********************************************************************/


/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
namespace DBE
{
	class MeshBuilder {
		public:
			/// Constructor.
			MeshBuilder();
			/// Destructor.
			~MeshBuilder();
			
			
		private:
			
			
			/// Private copy constructor to prevent accidental multiple instances.
			MeshBuilder( const DBE::MeshBuilder& other);
			/// Private assignment operator to prevent accidental multiple instances.
			DBE::MeshBuilder& operator=( const DBE::MeshBuilder& other);
			
	};
}  // namespace DBE

/*******************************************************************/
#endif	// #ifndef DBEMeshBuilderH
