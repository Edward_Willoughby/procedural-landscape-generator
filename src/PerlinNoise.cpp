/********************************************************************
*	Function definitions for the PerlinNoise class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "PerlinNoise.h"

#include <algorithm>
#include <numeric>
#include <random>

#include <DBEMath.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/


/**
* Constructor.
*/
PerlinNoise::PerlinNoise( u32 seed) {
	m_perm.resize( 256);

	std::iota( m_perm.begin(), m_perm.end(), 0);

	std::default_random_engine engine( seed);

	std::shuffle( m_perm.begin(), m_perm.end(), engine);

	m_perm.insert( m_perm.end(), m_perm.begin(), m_perm.end());
}

/**
* Destructor.
*/
PerlinNoise::~PerlinNoise() {}

/**
* 
*
* @param :: 
*
* @return 
*/
double PerlinNoise::Noise( double x, double y, double z) {
	s32 xPos = (s32)DBE_Floor( x) & 255;
	s32 yPos = (s32)DBE_Floor( y) & 255;
	s32 zPos = (s32)DBE_Floor( z) & 255;

	x -= DBE_Floor( x);
	y -= DBE_Floor( y);
	z -= DBE_Floor( z);

	double u = Fade( x);
	double v = Fade( y);
	double w = Fade( z);

	s32 a = m_perm[xPos] + yPos;
	s32 aa = m_perm[a] + zPos;
	s32 ab = m_perm[a + 1] + zPos;
	s32 b = m_perm[xPos + 1] + yPos;
	s32 ba = m_perm[b] + zPos;
	s32 bb = m_perm[b + 1] + zPos;

	double res = Lerp(w,
					Lerp(v,
						Lerp(u, Grad( m_perm[aa], x, y, z), Grad( m_perm[ba], x-1, y, z)),
						Lerp(u, Grad( m_perm[ab], x, y-1, z), Grad( m_perm[bb], x-1, y-1, z))),
					Lerp(v,
						Lerp(u, Grad( m_perm[aa+1], x, y, z-1), Grad( m_perm[ba+1], x-1, y, z-1)),
						Lerp(u, Grad( m_perm[ab+1], x, y-1, z-1), Grad( m_perm[bb+1], x-1, y-1, z-1))));

	return (res + 1.0f) / 2.0f;
}

double PerlinNoise::Fade( double t) {
	return t * t * t * (t * (t * 6 - 15) + 10);
}

double PerlinNoise::Lerp( double t, double a, double b) {
	return a + t * (b - a);
}

double PerlinNoise::Grad( s32 hash, double x, double y, double z) {
	s32 h = hash & 15;
	double u = h < 8 ? x : y,
		v = h < 4 ? y : h == 12 || h == 14 ? x : z;
	return ((h & 1) == 0 ? u : -u) + ((h &2) == 0 ? v : -v);
}