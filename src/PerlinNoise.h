/********************************************************************
*
*	CLASS		:: PerlinNoise
*	DESCRIPTION	:: Generates an array of values based on the Perlin Noise algorithm (i.e. difference
*					clouds filter from Photoshop).
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 07 / 03
*	SOURCE		:: https://solarianprogrammer.com/2012/07/18/perlin-noise-cpp-11/
*
********************************************************************/

#ifndef PerlinNoiseH
#define PerlinNoiseH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <vector>

#include <DBETypes.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class PerlinNoise {
	public:
		/// Constructor.
		PerlinNoise( u32 seed);
		/// Destructor.
		~PerlinNoise();
		
		double Noise( double x, double y, double z);
		
	private:
		double Fade( double t);
		double Lerp( double t, double a, double b);
		double Grad( s32 hash, double x, double y, double z);

		std::vector<s32> m_perm;
		
		/// Private copy constructor to prevent accidental multiple instances.
		PerlinNoise( const PerlinNoise& other);
		/// Private assignment operator to prevent accidental multiple instances.
		PerlinNoise& operator=( const PerlinNoise& other);
		
};

/*******************************************************************/
#endif	// #ifndef PerlinNoiseH
