/********************************************************************
*	Function definitions for the LandscapeGenerator class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "LandscapeGenerator.h"

#include <DBEBasicMesh.h>
#include <DBECamera.h>
#include <DBEColour.h>
#include <DBEFont.h>

#include "Landscape.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

static u32 gs_frames = 0;
static float gs_deltaTotal = 0.0f;


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool LandscapeGenerator::HandleInit() {
	mp_cam = new Camera();
	//mp_cam->m_position = Vec3( 0.0f, 10.0f, -10.0f);
	//mp_cam->m_position = Vec3( DBE_Sin( m_phi), 10.0f, DBE_Cos( m_phi));
	mp_cam->m_lookAt = Vec3( 0.0f, 0.0f, 0.0f);
	m_phi = 0.0f;
	m_rotateCam = true;

	mp_landscape = new Landscape();

	mp_test = new BasicMesh( MT_Sphere, 1.0f, 100.0f, 100.0f);
	mp_test->MoveTo( 0.0f, 5.0f, 0.0f);
	mp_test2 = new BasicMesh( MT_Sphere, 1.0f, 100.0f, 100.0f);
	mp_test2->MoveTo( 0.0f, 0.0f, 0.0f);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 1.0f, 1.0f, 1.0f)); //0.75f, 0.75f, 0.85f));

	m_wireframe = false;

	m_windowTitle = "Landscape Generator";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool LandscapeGenerator::HandleInput() {
	// Toggle wireframe mode.
	if( MGR_INPUT().KeyPressed( 'W'))
		m_wireframe = !m_wireframe;

	// Quit the program.
	if( MGR_INPUT().KeyPressed( VK_ESCAPE))
		this->Terminate();

	static bool s_capFPS( true);
	if( MGR_INPUT().KeyPressed( 'F')) {
		s_capFPS = !s_capFPS;

		if( s_capFPS)
			FRAME_TIMER().SetFPS( true, 60);
		else
			FRAME_TIMER().SetFPS( false);
	}

	static float mov( 0.1f);
	if( MGR_INPUT().KeyHeld( VK_UP))
		mp_test->MoveBy( mov, 0.0f, 0.0f);
	if( MGR_INPUT().KeyHeld( VK_DOWN))
		mp_test->MoveBy( -mov, 0.0f, 0.0f);
	if( MGR_INPUT().KeyHeld( VK_LEFT))
		mp_test->MoveBy( 0.0f, 0.0f, mov);
	if( MGR_INPUT().KeyHeld( VK_RIGHT))
		mp_test->MoveBy( 0.0f, 0.0f, -mov);

	//if( MGR_INPUT().KeyPressed( 'Q')) {
	//	SafeDelete( mp_test);
	//	mp_test = new BasicMesh( MT_Sphere, 1.0f, 100.0f, 100.0f);
	//	mp_test->MoveTo( mp_landscape->GetVertPos( 0, 0));
	//}
	//if( MGR_INPUT().KeyPressed( 'A')) {
	//	SafeDelete( mp_test2);
	//	mp_test2 = new BasicMesh( MT_Sphere, 1.0f, 10.0f, 10.0f);
	//	mp_test2->MoveTo( mp_landscape->GetVertPos( 5, 0));
	//}
	if( MGR_INPUT().KeyPressed( 'E')) {
		LandscapeSettings s;
		s.height = 20;
		s.width = 20;
		s.hilly = 0;

		mp_landscape->SetLandscapeSettings( s);
		mp_landscape->GenerateLandscape();
	}

	if( MGR_INPUT().KeyPressed( 'R'))
		m_rotateCam = !m_rotateCam;

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool LandscapeGenerator::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	if( m_rotateCam)
		m_phi += 0.01f;

	float dist( 20.0f);
	mp_cam->m_position = Vec3( DBE_Sin( m_phi) * dist, 10.0f, DBE_Cos( m_phi) * dist);
	mp_cam->Update();

	Vec3 colPos;
	if( mp_landscape->FindHeightmapPoint( mp_test->GetPosition(), colPos))
		mp_test2->MoveTo( colPos);

	mp_landscape->Update( deltaTime);
	
	if( mp_test != nullptr)
		mp_test->Update( deltaTime);
	if( mp_test2 != nullptr)
		mp_test2->Update( deltaTime);

	++gs_frames;
	gs_deltaTotal += deltaTime;

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool LandscapeGenerator::HandleRender( float deltaTime) {
	this->HandleRender3D( deltaTime);
	this->HandleRender2D( deltaTime);

	// Re-apply the 3D projection matrix and view matrix so they're stored for the next update and
	// can thus be used for 'picking'.
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 1000.0f);
	mp_cam->ApplyViewMatrixLH();

	return true;
}

/**
* Deals with any 2D rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void LandscapeGenerator::HandleRender2D( float deltaTime) {
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 1000.0f);

	GET_APP()->SetBlendState( true);
	GET_APP()->SetDepthStencilState( true, true);
	GET_APP()->SetRasteriserState( false, m_wireframe);

	// Debug text.
	DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Age: %i", mp_landscape->GetLandscapesAge());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.250f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Highlight: %f", mp_level->GetHoveredTileHightlight());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.225f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Building Hovered: %i", mp_ui->GetSelectedOption());
	
	//Vec2 pos, screen;
	//if( MGR_INPUT().GetMousePos( pos)) {
	//	if( pos.GetX() != -1 && pos.GetY() != -1) {
	//		screen.SetX( (pos.GetX() / SCREEN_WIDTH) - 0.5f);
	//		screen.SetY( ((pos.GetY() / SCREEN_HEIGHT) - 0.5f) * -1.0f);
	//	}
	//}
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.125f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "X: %f, Y: %f\n", pos.GetX(), pos.GetY());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.100f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "X: %f, Y: %f\n", screen.GetX(), screen.GetY());
}

/**
* Deals with any 3D rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void LandscapeGenerator::HandleRender3D( float deltaTime) {
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 1000.0f);
	mp_cam->ApplyViewMatrixLH();

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, m_wireframe);

	mp_landscape->Render( deltaTime);
	
	//if( mp_test != nullptr)
	//	mp_test->Render( deltaTime);
	//if( mp_test2 != nullptr)
	//	mp_test2->Render( deltaTime);
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool LandscapeGenerator::HandleShutdown() {
	SafeDelete( mp_cam);
	SafeDelete( mp_landscape);

	SafeDelete( mp_test);
	SafeDelete( mp_test2);

	DebugTrace( "Frames: %i\nDelta Average: %f\nAverage F.P.S.: %f\n", gs_frames, gs_deltaTotal / gs_frames, gs_frames / gs_deltaTotal);

	return true;
}